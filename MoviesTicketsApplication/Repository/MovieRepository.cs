﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;
using MoviesTicketsApplication.Models;

namespace MoviesTicketsApplication.DAO
{
    public class MovieRepository : IMovieRepository
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        protected static String conn = "mongodb://localhost:27017";
        private static IMongoCollection<Movie> moviesCollection;
  
        public MovieRepository()
        {
            _client = new MongoClient(conn);
            _database = _client.GetDatabase("movies_tickets_application");
            moviesCollection = _database.GetCollection<Movie>("movie");
        }

        public List<Movie> listAllMovies()
        {
            var filter = new BsonDocument();
            List<Movie> movies = new List<Movie>();

            try
            {
                var documents = moviesCollection.Find(filter).ToList();
                movies = documents.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred when trying to retrieve the movies.", e);
            }
            return movies;
        }
    }
}