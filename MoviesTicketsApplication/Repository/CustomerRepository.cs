﻿using log4net;
using MongoDB.Bson;
using MongoDB.Driver;
using MoviesTicketsApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace MoviesTicketsApplication.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        protected static String conn = "mongodb://localhost:27017";
        private static IMongoCollection<Customer> customersCollection;
        private static readonly ILog logger = LogManager.GetLogger(typeof(CustomerRepository));
        private const string CacheKey = "CustomerStoreCache";

        public CustomerRepository()
        {
            _client = new MongoClient(conn);
            _database = _client.GetDatabase("movies_tickets_application");
            customersCollection = _database.GetCollection<Customer>("customer");
        }

        public List<Customer> listAllCustomers()
        {
            var filter = new BsonDocument();
            List<Customer> customers = new List<Customer>();
            try
            {
                var documents = customersCollection.Find(filter).ToList();
                customers = documents.ToList();
            }
            catch (Exception e)
            {
                logger.Error("An error occurred when trying to retrieve the customers.", e);
            }
            return customers;
        }

        public Customer getCustomerByIdentification(string cpf)
        {
            Customer result = new Customer();
            try
            {   
                if (cpf.Equals(null) || (cpf.Equals("")))
                {
                    throw new ArgumentNullException();
                }
                var filter = Builders<Customer>.Filter.Eq(c => c.cpf, cpf);
                var document = customersCollection.Find(filter);
                result = document.Single();

            } catch (Exception e)
            {
                logger.Info("The identification is null. Could not retrieve the data.", e);
            }
            return result;
        }

        public Customer addNewCustomer(Customer customer)
        {
            try
            {
                if (customer == null)
                {
                    throw new ArgumentNullException();
                }

                customersCollection.InsertOne(customer);

            } catch (ArgumentNullException e)
            {
                logger.Info("The object is null. Could not be inserted.", e);
            }
            return customer;
        }

        public bool deleteCustomer(string cpf)
        {
            bool result = false;
            try
            {
                var filter = Builders<Customer>.Filter.Eq(c => c.cpf, cpf);
                var deleteResult = customersCollection.FindOneAndDelete(filter);
                if (deleteResult != null) { result = true; }
            } catch (Exception e)
            {
                logger.Error("It was not possible to remove the customer.", e);
            }
            return result;
        }

        public bool updateCustomer(Customer customer)
        {
            bool result = false;
            Customer updateCustomer = null;
            try
            {
                var filter = Builders<Customer>.Filter.Eq(c => c.cpf, customer.cpf);
                updateCustomer = customersCollection.FindOneAndReplace(filter, customer);
                if (updateCustomer != null) { result = true; }
            } catch (Exception e)
            {
                logger.Error("It was not possible to update the customer.", e);
            }
            return result;
        }
    }
}