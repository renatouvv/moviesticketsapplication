﻿using MongoDB.Bson;
using MoviesTicketsApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesTicketsApplication.Repository
{
    public interface ICustomerRepository
    {
        List<Customer> listAllCustomers();
        Customer getCustomerByIdentification(string cpf);
        Customer addNewCustomer(Customer customer);
        bool deleteCustomer(string customer);
        bool updateCustomer(Customer customer);
    }
}
