﻿using MoviesTicketsApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesTicketsApplication.DAO
{
    public interface IMovieRepository
    {
        List<Movie> listAllMovies();
    }
}
