﻿using MoviesTicketsApplication.DAO;
using MoviesTicketsApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ninject;
using System.Reflection;

namespace MoviesTicketsApplication.Controllers
{
    public class MoviesController : ApiController
    {
        IMovieRepository _movieRepository;
        public MoviesController (IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }
        public MoviesController () { }
                
        // GET api/movies
        [AcceptVerbs("GET")]
        public List<Movie> listAllMovies()
        {
            List<Movie> movies = new List<Movie>();
            try
            {
                movies = _movieRepository.listAllMovies();
            } catch (Exception e)
            {
                Console.WriteLine("A problem occurred when tryng to retrieve the list of all movies.", e);
            }
            return movies;
        }

    }
}
