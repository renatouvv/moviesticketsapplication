﻿using GatewayApiClient;
using GatewayApiClient.Utility;
using log4net;
using MoviesTicketsApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;

namespace MoviesTicketsApplication.Controllers
{
    public class TransactionsController : ApiController
    {
        private static string merchantKey = WebConfigurationManager.AppSettings["GatewayService.MerchantKey"];
        private static string mainURI = WebConfigurationManager.AppSettings["GatewayService.HostUri"];
        private static IGatewayServiceClient _gatewayClient;
        private static readonly ILog logger = LogManager.GetLogger(typeof(TransactionsController));
        private static HttpRequestMessage request;

        public TransactionsController (IGatewayServiceClient gatewayClient) {
            _gatewayClient = gatewayClient;
            request = new HttpRequestMessage();
            request.RequestUri = new Uri(mainURI);
            request.Content = new StringContent("", Encoding.UTF8, "application/json");
            request.Content.Headers.Clear();
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Headers.Clear();
            request.Headers.TryAddWithoutValidation("Content-Type", "application/json");
            //request.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            request.Headers.Add("MerchantKey", merchantKey);
            //client.BaseAddress = new Uri(mainURI);
            //client.DefaultRequestHeaders.Clear();
            //client.DefaultRequestHeaders.Add("Accept", "application/json");
            //client.DefaultRequestHeaders.Add("Content-Type", "application/json; charset=utf-8");
            //client.DefaultRequestHeaders.Add("MerchantKey", merchantKey);
        }
        public TransactionsController () { }
        // POST api/orders/order/{orderNumber}
        [AcceptVerbs("GET")]
        [ActionName("order")]
        public HttpResponseMessage getOrder(string order)
        {
            Guid merchantKeyToGuid = Guid.Parse(merchantKey);

            //var request = new HttpRequestMessage().Headers.;
            request.SetConfiguration(new HttpConfiguration());
            
            IGatewayServiceClient gatewayClient = new GatewayServiceClient(merchantKeyToGuid, new Uri(mainURI));
            var httpResponse = gatewayClient.Sale.QueryOrder(order);

            if (httpResponse.HttpStatusCode == HttpStatusCode.OK)
            {
                var sale = httpResponse.Response.SaleDataCollection.First();
                return request.CreateResponse(sale.OrderData.OrderReference);
            }
            return request.CreateErrorResponse(System.Net.HttpStatusCode.BadRequest, "There was a problem trying to get this order.");
        }

    }
}
