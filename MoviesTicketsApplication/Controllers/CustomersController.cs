﻿using log4net;
using MoviesTicketsApplication.Models;
using MoviesTicketsApplication.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoviesTicketsApplication.Controllers
{
    public class CustomersController : ApiController
    {
        ICustomerRepository _customerRepository;
        private static readonly ILog logger = LogManager.GetLogger(typeof(CustomersController));
        public CustomersController (ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }
        public CustomersController() { }

        // GET api/customers/all
        //[ActionName("all")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage listAllCustomers()
        {
            var request = new HttpRequestMessage();
            request.SetConfiguration(new HttpConfiguration());
            List<Customer> customers = null;            
            try
            {
                customers = _customerRepository.listAllCustomers();
                var response = request.CreateResponse<List<Customer>>(System.Net.HttpStatusCode.OK, customers);
                response.EnsureSuccessStatusCode();
                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine("A problem occurred when tryng to retrieve the list of all movies.", e);
                return request.CreateResponse<string>(System.Net.HttpStatusCode.BadRequest.ToString());
            }
        }

        // GET api/customers/one/{cpf}
        //[ActionName("one")]
        [AcceptVerbs("GET")]
        public Customer getCustomerByIdentification(string cpf)
        {
            Customer customer = new Customer();
            try
            {
                customer = _customerRepository.getCustomerByIdentification(cpf);
            } catch (Exception e)
            {
                logger.Error("It was not possible to retrieve the user.", e);
            }
            return customer;
        }

        // POST api/customers/add
        [AcceptVerbs("POST")]
        public HttpResponseMessage addNewCustomer(Customer customer)
        {
            var request = new HttpRequestMessage();
            request.SetConfiguration(new HttpConfiguration());
            Customer newCustomer = null;
            try
            {
                if (customer == null)
                {
                    throw new ArgumentNullException();
                }
                newCustomer = _customerRepository.addNewCustomer(customer);
                var response = request.CreateResponse<Customer>(System.Net.HttpStatusCode.Created, newCustomer);
                response.EnsureSuccessStatusCode();
                return response;
            } catch (Exception e)
            {
                logger.Error("it was not possible to add the new customer.", e);
                return request.CreateErrorResponse(System.Net.HttpStatusCode.Forbidden, e);
            }
            
        }

        // POST api/customers/update
        [AcceptVerbs("PUT")]
        public HttpResponseMessage updateCustomer(Customer customer)
        {
            var request = new HttpRequestMessage();
            request.SetConfiguration(new HttpConfiguration());
            bool result = false;
            try
            {
                result = _customerRepository.updateCustomer(customer);
                if (result)
                {
                    var response = request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Customer Successfully updated.");
                    response.EnsureSuccessStatusCode();
                    return response;
                } else
                {
                    return request.CreateResponse<string>(System.Net.HttpStatusCode.NotFound, "Customer not Found.");
                }
            } catch (Exception e)
            {
                logger.Info("It was not possible to update the customer. Please consult the log.", e);
                return request.CreateErrorResponse(System.Net.HttpStatusCode.Forbidden, e);
            }
        }

        // DELETE api/customers/delete/{cpf}
        [AcceptVerbs("DELETE")]
        public HttpResponseMessage deleteCustomer(string cpf)
        {
            var request = new HttpRequestMessage();
            request.SetConfiguration(new HttpConfiguration());
            bool result = false;
            try
            {
                result = _customerRepository.deleteCustomer(cpf);
                if (result)
                {
                    var response = request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Customer Successfully removed.");
                    response.EnsureSuccessStatusCode();
                    return response;
                } else
                {
                    return request.CreateResponse<string>(System.Net.HttpStatusCode.NotFound, "Customer not Found.");
                }
            } catch (Exception e)
            {
                logger.Error("There was an error trying to remove the customer. Try again later.", e);
                return request.CreateErrorResponse(System.Net.HttpStatusCode.Forbidden, e);
            }
        }
    }
}
