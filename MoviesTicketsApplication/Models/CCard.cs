﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviesTicketsApplication.Models
{
    public class CCard
    {
        [BsonElement("cCardNumber")]
        public string cCardNumber { get; set; }
        [BsonElement("cCardBrand")]
        public string cCardBrand { get; set; }
        [BsonElement("amount")]
        public int amount { get; set; }
        [BsonElement("expMonth")]
        public int expMonth { get; set; }
        [BsonElement("expYear")]
        public int expYear { get; set; }
        [BsonElement("securityCode")]
        public int securityCode { get; set; }
        [BsonElement("holderName")]
        public string holderName { get; set; }
        [BsonElement("installmentCount")]
        public int installmentCount { get; set; }
        [BsonElement("orderNumber")]
        public string orderNumber { get; set; }
        [BsonElement("merchantKey")]
        public string merchantKey { get; set; }
        [BsonElement("instantBuyKey")]
        public string instantBuyKey { get; set; }
        [BsonElement("cCardBin")]
        public string cCardBin { get; set; }
    }
}