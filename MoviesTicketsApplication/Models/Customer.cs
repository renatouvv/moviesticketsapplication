﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviesTicketsApplication.Models
{
    public class Customer
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id;
        [BsonElement("firstName")]
        public string firstName { get; set; }
        [BsonElement("lastName")]
        public string lastName { get; set; }
        [BsonElement("cpf")]
        public string cpf { get; set; }
        [BsonElement("birthday")]
        public string birthday { get; set; }
        [BsonElement("gender")]
        public string gender { get; set; }
        [BsonElement("creditCard")]
        public CCard creditCard { get; set; }
    }
}