﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviesTicketsApplication.Models
{
    public class Order
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id;
        [BsonElement("orderNumber")]
        public string orderNumber { get; set; }
        [BsonElement("paymentSource")]
        public string paymentSource { get; set; }
        [BsonElement("movie")]
        public Movie movie { get; set; }
        [BsonElement("amount")]
        public double amount { get; set; }
        [BsonElement("cCardBrand")]
        public string cCardBrand { get; set; }
        [BsonElement("cCardBin")]
        public string cCardBin { get; set; }
        [BsonElement("installmentCount")]
        public string installmentCount { get; set; }
    }
}