﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace MoviesTicketsApplication.Models
{
    public class Movie
    {
        [BsonId]
        public ObjectId _id;
        public string name { get; set; }
        public int rating { get; set; } 
        public string synopsis { get; set; }
        public string distributor { get; set; }
        public string director { get; set; }
        public decimal price { get; set; }
    }
}